# Java Assignment 1 - RPG Characters and Items
This project was created as part of an assigment in Java. The application allows for the user to choose different characters and equip different items to them. The user can choose to level up the character which increases its stats.

## Installation 

### Downloading
The application is free to clone straight from gitlab. Type this into your selected git console to get the current main version: 
```
git clone git@gitlab.com:AdrianMattsson/first-java-assignment.git
```
Git will copy the repository onto your machine.


## Usage
### Running the application
The application is very simple to run. Just compile it in your IDE of choice to test the test results.
Want to modify it to your liking?
Just edit the Main.java to add the items or characters you wish to have. Alternatively create a new class with your alterations and then use it in Main.java.


### Useful methods
There a few useful methods you can use to enjoy the application.
For the character classes you can use the following methods:
* levelUp()             -   Levels up the character and increases its stats.
* equip()               -   Equips the character with the chosen item.
* displayInformation()   -   Displays information about the character and its abilities in the console.
* displayEquippedItems() -   Displays information about the characters currently equipped items.


## Contributors

Adrian Mattsson @AdrianMattsson
