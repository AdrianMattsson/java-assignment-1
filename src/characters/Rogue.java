package characters;

import items.armor.ArmorType;
import items.weapons.WeaponType;

public class Rogue extends Character {

    public Rogue (String name) {
        super(name, new PrimaryAttributes(2, 6, 1));
        //Allowed types of items to be equipped to this character. Other items will NOT be able to be equipped to this character
        setAllowedWeapons(new WeaponType[]{
                WeaponType.Dagger,
                WeaponType.Sword});
        setAllowedArmor(new ArmorType[]{
                ArmorType.Mail,
                ArmorType.Leather});
    }

    public void levelUp() {
        levelUp(1,4,1);
    }

    //Multiplies the characters damage with its special attribute
    @Override
    public float getDamageMultiplier(float damagePerSecond) {
        return damagePerSecond * (1 + getTotalAttributes().getDexterity()/100f);
    }
}
