package characters;

import items.armor.ArmorType;
import items.weapons.WeaponType;

public class Warrior extends Character {

    public Warrior (String name) {
        super(name, new PrimaryAttributes(5, 2, 1));
        //Allowed types of items to be equipped to this character. Other items will NOT be able to be equipped to this character
        setAllowedWeapons(new WeaponType[]{
                WeaponType.Axe,
                WeaponType.Hammer,
                WeaponType.Sword});
        setAllowedArmor(new ArmorType[]{
                ArmorType.Mail,
                ArmorType.Plate});
    }

    public void levelUp() {
        levelUp(3,2,1);
    }

    //Multiplies the characters damage with its special attribute
    @Override
    public float getDamageMultiplier(float damagePerSecond) {
        return damagePerSecond * (1 + getTotalAttributes().getStrength()/100f);
    }
}
