package characters.errors;

public class InvalidLevelException extends Exception{
    //Used when the chosen item is not allowed to be equipped to the chosen character
    public InvalidLevelException(String errorMessage) {
        super(errorMessage);
    }
}
