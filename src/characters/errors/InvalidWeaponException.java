package characters.errors;

public class InvalidWeaponException extends Exception{
    //Used when the chosen weapon is not allowed to be equipped to the chosen character
    public InvalidWeaponException(String errorMessage) {
        super(errorMessage);
    }
}
