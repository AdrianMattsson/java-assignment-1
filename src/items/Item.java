package items;

import characters.PrimaryAttributes;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private ItemSlot itemSlot;
    private PrimaryAttributes attributes = new PrimaryAttributes(); //Often only used when armor is the current item. While uncommon, certain weapons might increase attributes as well.

    public Item(String name, int requiredLevel, ItemSlot itemSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
    }

    public Item(String name, int requiredLevel, int addsStrength, int addsDexterity, int addsIntelligence, ItemSlot itemSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
        attributes.setToAttributes(addsStrength, addsDexterity, addsIntelligence);
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public String getName() {
        return name;
    }

    public abstract Object getType();  //Used to get ArmorType or WeaponType

    public ItemSlot getItemSlot() {
        return itemSlot;
    }

    public int getRequiredLevel() {
        return  requiredLevel;
    }
}
