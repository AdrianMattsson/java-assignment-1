package items;

public enum ItemSlot {
    //"Slots" on the character where equipment can be equipped
    Head,
    Body,
    Legs,
    Weapon,
}
