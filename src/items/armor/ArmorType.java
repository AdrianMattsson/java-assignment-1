package items.armor;

public enum ArmorType {
    //Different types of armor that can exist
    Cloth,
    Leather,
    Mail,
    Plate,
}
