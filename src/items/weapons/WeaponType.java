package items.weapons;

public enum WeaponType {
    //Different types of weapons that can exist
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand,
}
